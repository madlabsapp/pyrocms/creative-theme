<?php

use Anomaly\NavigationModule\Menu\MenuModel;

return [
    'enabled_header'    => [
        'type'       => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                '1' => 'newebtime.theme.creative::setting.enabled_header.options.every_page',
                '2' => 'newebtime.theme.creative::setting.enabled_header.options.homepage_only',
                '3' => 'newebtime.theme.creative::setting.enabled_header.options.disabled',
            ],
            'default_value' => '2'
        ],
    ],
    'navigation_menu' => [
        'type'   => 'anomaly.field_type.relationship',
        'config' => [
            'title_name' => 'name',
            'related'    => MenuModel::class,
        ],
    ],
    'inline_assets' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'yes' => 'newebtime.theme.creative::setting.inline_assets.options.yes',
                'no'  => 'newebtime.theme.creative::setting.inline_assets.options.no',
            ],
            'default_value' => 'no'
        ],
        'required' => true
    ],
];

<?php

return [
    'title'       => 'Creative',
    'name'        => 'Creative Theme',
    'description' => 'A theme based on the <a href="http://startbootstrap.com" target="_blank">Creative Theme</a>',
];

<?php

return [
    'enabled_header' => [
        'name'    => 'Enabled Header',
        'options' => [
            'every_page'    => 'Every Page',
            'homepage_only' => 'Homepage Only',
            'disabled'      => 'Disabled',
        ]
    ],
    'navigation_menu' => [
        'name' => 'Navigation Menu'
    ],
    'inline_assets' => [
        'name'    => 'Inline Assets',
        'options' => [
            'yes' => 'Yes',
            'no'  => 'No',
        ]
    ],
];
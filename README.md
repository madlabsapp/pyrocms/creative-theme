# Creative Theme

This theme is a basic HTML5 Bootstrap theme based on the Creative Theme from https://startbootstrap.com/template-overviews/creative/

## Installation

```bash
composer require newebtime/creative-theme
```

For more details: https://pyrocms.com/documentation/pyrocms/3.4/installation/installing-addons

## Customization

The theme come with a pre-compiled CSS file, but it's also providing the SCSS to customise the theme.

### Install dependencies

#### Manually

Go to the theme folder and run `npm install`

#### Using minstall

Minstall allow to automatically install `package.json` dependencies from all addons.

```bash
npm install minstall --save
```

Then update PyroCMS `package.json`

```json
    "scripts": {
        ...
        "postinstall": "minstall addons/*/*"
    },
```

Then

```bash
npm install
```


### Using Assetic

1. Go in `resources/views/partials/metadata.twig`
2. Comment `{{ asset_add('theme.css', 'theme::css/theme.css', ['live', 'parse']) }}`
3. Uncomment `{#{{ asset_add('theme.css', 'theme::scss/theme.scss', ['live', 'parse']) }}#}`

### Using Mix / Webpack

1. Go in `resources/scss/theme.scss`
2. Comment all the `// Vendor - Assetic` section
3. Uncomment all the `// Vendor - Mix` section

Then configure `webpack.mix.js` to compile the scss

```js
require(`./addons/app/newebtime/creative-theme/webpack.mix.js`);
```

Caution: `addons/app` may differ depending of you application reference.

## Tested / Compatible Module

* Posts Module (anomaly.module.posts), `2.4` (`2.5` is not tested yet)
  * Know bug: The `Tags` are not rendered correctly Too many twig need to be reworked for it...

## TODO

* Rework the JS (like we did for CSS/SCSS)
* package.json should just required `startbootstrap-creative`
  * So the SCSS should import the original SCSS
  * Same for the JS
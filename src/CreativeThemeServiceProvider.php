<?php namespace Newebtime\CreativeTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

/**
 * Class CreativeThemeServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CreativeThemeServiceProvider extends AddonServiceProvider
{
    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        'streams::errors/404'           => 'theme::errors/404',
        'streams::errors/500'           => 'theme::errors/500',
        'streams::form/partials/layout' => 'theme::form/partials/layout',
    ];
}
